<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::get('create_motorbike', 'admin\MotorbikeController@add')->name('create_m');
    Route::post('create_m', 'admin\MotorbikeController@create')->name('create');

});

    Route::get('lists/{sortBye}', 'web\MotorbikeController@view')->name('lists');
    Route::get('list_bye_color/{color}', 'web\MotorbikeController@filterByeColor')->name('filter_color');
    Route::get('details/{motor}', 'web\MotorbikeController@details')->name('detail');

