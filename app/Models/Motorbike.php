<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Motorbike extends Model
{
    //
    protected $fillable = [
        'model', 'color', 'weight', 'price', 'image'
    ];


}
