<?php

namespace App\Http\Controllers\web;

use App\Models\Motorbike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MotorbikeController extends Controller
{
    //
    public function view($sortBy = 'created_at')
    {
        $list = \App\Models\Motorbike::orderBy($sortBy, 'desc')->paginate(5);
        return view('lists')->with([
            'posts' => $list
        ]);


    }

    public function filterByeColor($color)
    {
        $posts = Motorbike::where('color', $color)->paginate(5);
        return view('lists')->with('posts', $posts);
    }


    public function details($post)
    {
        $post_ = Motorbike::findOrFail((int)$post);
        return view('details')->with('post',$post_);
    }
}
