<?php

namespace App\Http\Controllers\admin;

use App\Models\Motorbike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class MotorbikeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add()
    {
        return view('admin.create');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'model' => 'required|min:3',
            'image' => 'required',
            'price' => 'required',
        ], [
            'model.required' => 'model name in required'
        ]);
        try {
            $image = Input::file('image');
            $destinationPath = 'uploads';
            $extension = $image->extension();
            $fileName = rand(11111, 99999) . '.' . $extension;
            $urlbig = $image->move($destinationPath, $fileName);

            $args = [
                'model' => $request['model'],
                'color' => $request['color'],
                'weight' => (double)Input::get('weight'),
                'price' => (double)Input::get('price'),
                'image' => $urlbig
            ];
            $motor = Motorbike::create($args);

            if ($motor) {
                Session::flash('success', 'Record Created !');
            } else {
                Session::flash('error', 'something went wrong !!!');
            }

            return Redirect::back();

        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            return Redirect::back();
        }
    }
}
