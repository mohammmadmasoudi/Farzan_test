@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Motorbikes</div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">


                                <div class="col-md-12">
                                    <h4>Bootstrap Snipp for Datatable</h4>
                                    <div class="table-responsive">


                                        <table id="mytable" class="table table-bordred table-striped">

                                            <thead>

                                            <th><a href="{{ route('lists','model') }}">Model</a></th>
                                            <th><a href="{{ route('lists','color') }}">Color</a></th>
                                            <th>Weight</th>
                                            <th><a href="{{ route('lists','price') }}"> Price $</a></th>
                                            <th>Image</th>
                                            <th><a href="{{ route('lists','created_at') }}">Create at</th>
                                            <th>View</th>

                                            </thead>
                                            <tbody>
                                            @foreach($posts as $post)
                                                <tr>
                                                    <td>{{ $post->model }}</td>
                                                    <td><a href="{{ route('filter_color',$post->color) }}">{{ $post->color }}</a></td>
                                                    <td>{{ $post->weight }}</td>
                                                    <td>{{ $post->price }}</td>
                                                    <td>
                                                        @if($post->image)
                                                            <img src="{{ asset($post->image) }}" width="50">
                                                        @else
                                                            <img src="{{ asset('download.jpg') }}" width="50">

                                                        @endif
                                                    </td>
                                                    <td>{{ $post->created_at }}</td>

                                                    <td>
                                                        <a href="{{ route('detail',$post->id) }}">
                                                        <p data-placement="top" data-toggle="tooltip" title="Edit">
                                                            <button class="btn btn-primary btn-xs" data-title="Edit"
                                                                    data-toggle="modal" data-target="#edit"><span
                                                                        class="glyphicon glyphicon-pencil"></span>
                                                            </button>
                                                        </p>
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach


                                            </tbody>

                                        </table>

                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right">
                                            {{ $posts->links()  }}
                                        </ul>

                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection