@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Motorbike</div>
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                            class="sr-only">بستن</span></button>
                                {{Session::get('success')}}
                            </div>
                        @elseif(Session::has('error'))
                            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                            class="sr-only">بستن</span></button>
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-styled-left alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <span>×</span><span class="sr-only">بستن</span></button>
                                    {{ $error }}<br>
                                </div>
                            @endforeach
                        @endif


                        <form method="post" action="{{ route('create') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" class="form-control" id="model"
                                       aria-describedby="" placeholder="Enter Model" name="model" required>

                            </div>
                            <div class="form-group">
                                <label for="color">color</label>
                                <input type="text" class="form-control" id="color"
                                       aria-describedby="" placeholder="Enter color" name="color">

                            </div>
                            <div class="form-group">
                                <label for="color">weight</label>
                                <input type="number" class="form-control" id="color"
                                       aria-describedby="" placeholder="Enter weight G" name="weight">

                            </div>
                            <div class="form-group">
                                <label for="color">Price</label>
                                <input type="number" class="form-control" id="color"
                                       aria-describedby="" placeholder="Enter price $" name="price" required>

                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Upload Image</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image"
                                       required>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection