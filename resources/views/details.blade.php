@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Motorbike Details</div>
                    <div class="card-body">

                        <div class="form-group">
                            <label for="model">Model</label>
                            <input type="text" class="form-control" id="model" value="{{ $post->model }}"
                                   aria-describedby="" placeholder="Enter Model" name="model" disabled>

                        </div>
                        <div class="form-group">
                            <label for="color">color</label>
                            <input type="text" class="form-control" id="color" value="{{ $post->color }}"
                                   aria-describedby="" placeholder="Enter color" name="color" disabled>

                        </div>
                        <div class="form-group">
                            <label for="color">weight</label>
                            <input type="number" class="form-control" id="color" value="{{ $post->weight }}"
                                   aria-describedby="" placeholder="Enter weight G" name="weight" disabled>

                        </div>
                        <div class="form-group">
                            <label for="color">Price</label>
                            <input type="number" class="form-control" id="color" value="{{ $post->price }}"
                                   aria-describedby="" placeholder="Enter price $" name="price" disabled>

                        </div>
                        <div class="form-group">
                            @if($post->image)
                                <img src="{{ asset($post->image) }}" width="200">
                            @else
                                this post have not  image
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection